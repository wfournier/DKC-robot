# -*- coding: utf-8 -*-
{
'!langcode!': 'pt-br',
'!langname!': 'Português (do Brasil)',
'"update" is an optional expression like "field1=\'newvalue\'". You cannot update or delete the results of a JOIN': '"update" é uma expressão opcional como "campo1=\'novovalor\'". Você não pode atualizar ou apagar os resultados de um JOIN',
'%s %%{row} deleted': '%s linhas apagadas',
'%s %%{row} updated': '%s linhas atualizadas',
'%s selected': '%s selecionado',
'%Y-%m-%d': '%d-%m-%Y',
'%Y-%m-%d %H:%M:%S': '%d-%m-%Y %H:%M:%S',
'?': '?',
'@markmin\x01An error occured, please [[reload %s]] the page': 'An error occured, please [[reload %s]] the page',
'About': 'About',
'Access Control': 'Access Control',
'Add to mailing list?': 'Add to mailing list?',
'Administrative Interface': 'Administrative Interface',
'Administrative interface': 'Interface administrativa',
'Ajax Recipes': 'Ajax Recipes',
'appadmin is disabled because insecure channel': 'Administração desativada devido ao canal inseguro',
'Are you sure you want to delete this object?': 'Are you sure you want to delete this object?',
'Available Databases and Tables': 'Bancos de dados e tabelas disponíveis',
'Buy this book': 'Buy this book',
'cache': 'cache',
'Cache': 'Cache',
'Cache Cleared': 'Cache Cleared',
'Cache Keys': 'Cache Keys',
'Calculating scores': 'Calculating scores',
'Callsign': 'Callsign',
'Cannot be empty': 'Não pode ser vazio',
'category': 'category',
'Category': 'Category',
'Category mode': 'Category mode',
'Category region': 'Category region',
'Category type': 'Category type',
'change password': 'modificar senha',
'Check to delete': 'Marque para apagar',
'Claimed score': 'Claimed score',
'Clear CACHE?': 'Clear CACHE?',
'Clear DISK': 'Clear DISK',
'Clear RAM': 'Clear RAM',
'Client IP': 'Client IP',
'Close time': 'Close time',
'Community': 'Community',
'Components and Plugins': 'Components and Plugins',
'contests': 'contests',
'Controller': 'Controlador',
'Copyright': 'Copyright',
'Country': 'Country',
'Current request': 'Requisição atual',
'Current response': 'Resposta atual',
'Current session': 'Sessão atual',
'Currently, there is no active contest for which you can upload logs. Please come back later': 'Currently, there is no active contest for which you can upload logs. Please come back later',
'customize me!': 'Personalize-me!',
'data uploaded': 'dados enviados',
'Database': 'banco de dados',
'Database %s select': 'Selecionar banco de dados %s',
'Database Administration (appadmin)': 'Database Administration (appadmin)',
'Date and time': 'Date and time',
'db': 'bd',
'DB Model': 'Modelo BD',
'Delete:': 'Apagar:',
'Demo': 'Demo',
'Deployment Recipes': 'Deployment Recipes',
'Description': 'Description',
'design': 'design',
'DISK': 'DISK',
'Disk Cache Keys': 'Disk Cache Keys',
'Disk Cleared': 'Disk Cleared',
'Documentation': 'Documentation',
"Don't know what to do?": "Don't know what to do?",
'done!': 'concluído!',
'Download': 'Download',
'Dxcc country': 'Dxcc country',
'E-mail': 'E-mail',
'e-Mail address': 'e-Mail address',
'Edit': 'Editar',
'Edit current record': 'Editar o registro atual',
'edit profile': 'editar perfil',
'Edit This App': 'Edit This App',
'Email and SMS': 'Email and SMS',
'End time': 'End time',
'Enter an integer between %(min)g and %(max)g': 'Enter an integer between %(min)g and %(max)g',
'Enter an integer greater than or equal to %(min)g': 'Enter an integer greater than or equal to %(min)g',
'Errors': 'Errors',
'Expat motivatie': 'Expat motivatie',
'Expat motivation (if you qualify as one)': 'Expat motivation (if you qualify as one)',
'expats management': 'expats management',
'export as csv file': 'exportar como um arquivo csv',
'FAQ': 'FAQ',
'First name': 'First name',
'Forms and Validators': 'Forms and Validators',
'Free Applications': 'Free Applications',
'Frequency': 'Frequency',
'Full exchange text': 'Full exchange text',
'Graph Model': 'Graph Model',
'Group ID': 'Group ID',
'Groups': 'Groups',
'Hello World': 'Olá Mundo',
'here': 'here',
'Home': 'Home',
'How did you get here?': 'How did you get here?',
'import': 'import',
'Import/Export': 'Importar/Exportar',
'Index': 'Início',
'Info received': 'Info received',
'Info sent': 'Info sent',
'insert new': 'inserir novo',
'insert new %s': 'inserir novo %s',
'Internal State': 'Estado Interno',
'Introduction': 'Introduction',
'INVALID': 'INVALID',
'Invalid email': 'Invalid email',
'Invalid Query': 'Consulta Inválida',
'invalid request': 'requisição inválida',
'Key': 'Key',
'Last name': 'Last name',
'Layout': 'Layout',
'Layout Plugins': 'Layout Plugins',
'Layouts': 'Layouts',
'Live chat': 'Live chat',
'Live Chat': 'Live Chat',
'log detail': 'log detail',
'Log file to upload': 'Log file to upload',
'Log file to upload:': 'Log file to upload:',
'Log In': 'Log In',
'Log upload': 'Log upload',
'login': 'Entrar',
'Login': 'Autentique-se',
'logout': 'Sair',
'Lost Password': 'Esqueceu sua senha?',
'Lost password?': 'Lost password?',
'lost password?': 'lost password?',
'Main Menu': 'Menu Principal',
'Manage %(action)s': 'Manage %(action)s',
'Manage Access Control': 'Manage Access Control',
'Manage Cache': 'Manage Cache',
'Manage the contest': 'Manage the contest',
'manual score processing': 'manual score processing',
'Matching QSO record': 'Matching QSO record',
'Memberships': 'Memberships',
'Menu Model': 'Modelo de Menu',
'Mode': 'Mode',
'Multiplier': 'Multiplier',
'My Sites': 'My Sites',
'Name': 'Name',
'New Record': 'Novo Registro',
'new record inserted': 'novo registro inserido',
'next %s rows': 'next %s rows',
'next 100 rows': 'próximas 100 linhas',
'No databases in this application': 'Sem bancos de dados nesta aplicação',
'Not published yet': 'Not published yet',
'Online examples': 'Alguns exemplos',
'Operator': 'Operator',
'Operator callsign': 'Operator callsign',
'Opponent callsign': 'Opponent callsign',
'or import from csv file': 'ou importar de um arquivo csv',
'Origin': 'Origin',
'Original QSO line': 'Original QSO line',
'Other Plugins': 'Other Plugins',
'Other Recipes': 'Other Recipes',
'Overview': 'Overview',
'Password': 'Password',
'Permission': 'Permission',
'Permissions': 'Permissions',
'Please check the form for errors': 'Please check the form for errors',
'Please fill in the following form to submit your contest log': 'Please fill in the following form to submit your contest log',
'Please select a value from this list': 'Please select a value from this list',
'Please select your category': 'Please select your category',
'Plugins': 'Plugins',
'Points': 'Points',
'Powered by': 'Powered by',
'Preface': 'Preface',
'previous %s rows': 'previous %s rows',
'previous 100 rows': '100 linhas anteriores',
'Published': 'Published',
'pygraphviz library not found': 'pygraphviz library not found',
'Python': 'Python',
'Query:': 'Consulta:',
'Quick Examples': 'Quick Examples',
'RAM': 'RAM',
'RAM Cache Keys': 'RAM Cache Keys',
'Ram Cleared': 'Ram Cleared',
'Recipes': 'Recipes',
'Record': 'registro',
'record does not exist': 'registro não existe',
'Record ID': 'Record ID',
'Record id': 'id do registro',
'Register': 'Registre-se',
'register': 'Registre-se',
'Registration key': 'Registration key',
'Remember me (for 30 days)': 'Remember me (for 30 days)',
'Reset Password key': 'Reset Password key',
'Resources': 'Resources',
'Role': 'Role',
'Roles': 'Roles',
'Rows in Table': 'Linhas na tabela',
'Rows selected': 'Linhas selecionadas',
'Save model as...': 'Save model as...',
'scoring results': 'scoring results',
'Select contest': 'Select contest',
'Semantic': 'Semantic',
'Services': 'Services',
'Size of cache:': 'Size of cache:',
'Skipped QSOs': 'Skipped QSOs',
'Soap box': 'Soap box',
'Start time': 'Start time',
'state': 'estado',
'Statistics': 'Statistics',
'Stylesheet': 'Stylesheet',
'submit': 'submit',
'Submit': 'Submit',
'Support': 'Support',
'Sure you want to delete this object?': 'Está certo(a) que deseja apagar esse objeto ?',
'SWL': 'SWL',
'SWL log': 'SWL log',
'Table': 'tabela',
'Table name': 'Table name',
'The "query" is a condition like "db.table1.field1==\'value\'". Something like "db.table1.field1==db.table2.field2" results in a SQL JOIN.': 'Uma "consulta" é uma condição como "db.tabela1.campo1==\'valor\'". Expressões como "db.tabela1.campo1==db.tabela2.campo2" resultam em um JOIN SQL.',
'The Core': 'The Core',
'The DKARS team is busy processing the contest logs and has not published the results yet.': 'The DKARS team is busy processing the contest logs and has not published the results yet.',
'The output of the file is a dictionary that was rendered by the view %s': 'The output of the file is a dictionary that was rendered by the view %s',
'The Views': 'The Views',
'This App': 'This App',
'This is a copy of the scaffolding application': 'This is a copy of the scaffolding application',
'Time in Cache (h:m:s)': 'Time in Cache (h:m:s)',
'Timestamp': 'Timestamp',
'Total': 'Total',
'Total QSOs': 'Total QSOs',
'Traceback': 'Traceback',
'Transmitter ID': 'Transmitter ID',
'Twitter': 'Twitter',
'unable to parse csv file': 'não foi possível analisar arquivo csv',
'Update:': 'Atualizar:',
'Use (...)&(...) for AND, (...)|(...) for OR, and ~(...)  for NOT to build more complex queries.': 'Use (...)&(...) para AND, (...)|(...) para OR, e ~(...)  para NOT para construir consultas mais complexas.',
'User': 'User',
'User ID': 'User ID',
'User Voice': 'User Voice',
'Users': 'Users',
'Valid record': 'Valid record',
'Videos': 'Videos',
'View': 'Visualização',
'Web2py': 'Web2py',
'Welcome': 'Welcome',
'Welcome %s': 'Vem vindo %s',
'Welcome to web2py': 'Bem vindo ao web2py',
'Welcome to web2py!': 'Welcome to web2py!',
'Which called the function %s located in the file %s': 'Which called the function %s located in the file %s',
'Working...': 'Working...',
'You are successfully running web2py': 'You are successfully running web2py',
'You are successfully running web2py.': 'You are successfully running web2py.',
'You can modify this application and adapt it to your needs': 'You can modify this application and adapt it to your needs',
'You visited the url %s': 'You visited the url %s',
'Your log was successfully uploaded and will now be processed by the system.                 Once processed, you will receive a mail with the parsing results.': 'Your log was successfully uploaded and will now be processed by the system.                 Once processed, you will receive a mail with the parsing results.',
}
