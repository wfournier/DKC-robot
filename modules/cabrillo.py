#!/usr/bin/env python
# coding: utf8
from datetime import datetime
from dxcc import DXCC
import logging

logger = logging.getLogger('dkc.cabrillo')

# Should we use the DB? or should we get called as an object with a reference to the db?
# https://github.com/hsmade/DKC-robot/issues/2
from gluon import current

db = current.db


def get_band(freq, contest):
    """
    Get the band name by looking up the frequency in the db
    :return:
    boolean
    """
    if freq < 1000:
        freq *= 1000
    for band in db((db.t_bands_for_contest.f_contest == contest) &
                   (db.t_bands.id == db.t_bands_for_contest.f_band)).select(db.t_bands.ALL):
        if band['f_low'] <= freq <= band['f_high']:
            return band['f_name']
    return False


def verify_time(timestamp, contest):
    """
    Verify log entries' time against all contests' start and end time
    :return:
    boolean
    """
    for contest in db(db.t_contests.id == contest).select():
        if contest['f_start'] <= timestamp <= contest['f_end']:
            return True
    return False


def is_allowed_mode(mode):
    """
    Verify log entries' mode against allowed modes
    :return:
    boolean
    """
    for modeRow in db().select(db.t_modes.ALL):
        # logger.debug('cabrillo.verifyMode: {} == {}'.format(mode.upper(), modeRow['f_mode'].upper()))
        if mode.upper() == modeRow['f_mode'].upper():
            return True
    return False


class Cabrillo(object):
    """
    represents a Cabrillo format log file
    """

    def __init__(self, logfile, contest, category_mode):
        logger.debug('Cabrillo.__init__()')
        self.logfile = logfile
        self.contest = contest
        self.category_mode = category_mode
        self.DxCC = DXCC()
        # parser results:
        self.stats = dict(total=0, valid=0, invalid=[], warnings=[], bands=dict(undetermined=0), modes=dict())

    @property
    def parse_header(self):
        """
        Parses the Cabrillo header
        :return:
        A list of:
         * the operator
         * the country
         * the soapbox
         * the claimed score
        """
        logger.debug('Cabrillo.parseHeader()')
        soapbox = []
        operator = ''
        country = ''
        claimed_score = -1
        # file_pos = self.logfile.tell()
        for line in self.logfile.readlines():
            logger.debug('Cabrillo.parseHeader: read line: {}'.format(line))
            # add a check for the correct Cabrillo version (do we depend on any specific version?)
            # https://github.com/hsmade/DKC-robot/issues/3
            if line.startswith('CALLSIGN:'):
                operator = line.split(':')[1].strip().upper()
                country = self.DxCC.get_country(operator)
            if line.startswith('CLAIMED-SCORE:'):
                try:
                    claimed_score = int(line.split(':')[1].strip().replace(',', '').replace('.', ''))
                except Exception as e:
                    logger.warning('Cabrillo.parseheader: can\'t parse claimed score as number: {}'.format(line))
                    claimed_score = 0
            if line.startswith('SOAPBOX:'):
                soapbox.append(line.split(':')[1].rstrip('\n'))
            if line.startswith('QSO:'):
                logger.debug('Cabrillo.parseHeader: stopped at QSOs')
                break
                # file_pos = self.logfile.tell()
        if not operator:
            raise SyntaxError('Missing field "CALLSIGN:" in Cabrillo header')
        if not country:
            raise ValueError('Unable to parse your callsign {} into a country using the DXCC list. \
                              Please contact ict@dkars.nl'.format(operator))
        return operator, country, '\n'.join(soapbox), claimed_score

    def qsos(self):
        """
        returns a generator of qsos
        """
        logger.debug('Cabrillo.qsos()')
        logger.debug('Cabrillo.qsos: reset logfile')
        self.logfile.seek(0)
        for line in self.logfile.readlines():
            logger.debug('Cabrillo.qsos: handling line: {}'.format(line.strip()))
            line = line.rstrip('\n')
            if line.startswith('QSO:'):
                self.stats['total'] += 1
                valid = True
                logger.debug('Cabrillo.qsos: QSO found: {}'.format(line))
                '''
                                              --------info sent------- -------info rcvd--------
                QSO: freq  mo date       time call          rst exch   call          rst exch   t
                QSO: ***** ** yyyy-mm-dd nnnn ************* nnn ****** ************* nnn ****** n
                QSO:  3799 PH 1999-03-06 0711 HC8N          59  001    W1AW          59  001    0
                QSO: 28123 PH 2014-12-31 927  0A2CD         599 314    IW4ENE        599 314
                000000000111111111122222222223333333333444444444455555555556666666666777777777788
                123456789012345678901234567890123456789012345678901234567890123456789012345678901
                '''
                # try:
                #     if not (line[4] == ' ' or
                #             line[10] == ' ' or
                #             line[13] == ' ' or
                #             line[24] == ' ' or
                #             line[29] == ' ' or
                #             line[43] == ' ' or
                #             line[47] == ' ' or
                #             line[54] == ' ' or
                #             line[68] == ' ' or
                #             line[72] == ' '):
                #         logger.warning('Cabrillo.qsos: invalid QSO line'.format(line))
                #         self.stats['invalid'].append(line)
                #         continue  # skip to next line
                # except IndexError as e:
                #     logger.warning('Cabrillo.qsos: index error: {}'.format(e))
                #     self.stats['invalid'].append(line)
                #     continue  # skip to next line
                qso = {
                    'f_invalidation_reason': '',
                    'qsoLine': line,
                    'valid': True,
                    'fromCallsign': '',
                    'freq': '',
                    'mode': '',
                    'datetime': datetime(1970, 1, 1),
                    'callsign': '',
                    'exchange': '',
                    'transmitterID': '',
                    'infoSent': '',
                    'infoReceived': '',
                }
                if not 12 >= len(line.split()) >= 11:
                    reason = 'Invalid number of fields on QSO line'
                    logger.warning('Cabrillo.qsos: {reason}: {line}'.format(reason=reason, line=line))
                    self.stats['invalid'].append(line)
                    qso['valid'] = False
                    qso['f_invalidation_reason'] += reason
                    yield qso

                qso['fromCallsign'] = line.split()[5]  # line[30:43].strip()
                freq = line.split()[1].replace('.', '')
                try:
                    qso['freq'] = int(freq)  # line[5:10].strip()
                except Exception as error:
                    reason = 'Invalid frequency: {freq}'.format(freq=freq)
                    logger.warning('Cabrillo.qsos: {reason}: {line}'.format(reason=reason, line=line))
                    self.stats['invalid'].append('{reason}: {line}'.format(reason=reason, line=line))
                    qso['f_invalidation_reason'] += reason
                    qso['valid'] = False
                    qso['freq'] = 0
                band = get_band(qso['freq'], self.contest)
                if not band:
                    reason = 'Could not find band for frequency: {freq}'.format(freq=qso['freq'])
                    logger.warning('Cabrillo.qsos: {reason}: {line}'.format(reason=reason, line=line))
                    self.stats['invalid'].append('{reason}: {line}'.format(reason=reason, line=line))
                    qso['f_invalidation_reason'] += reason
                    qso['valid'] = False
                    self.stats['bands']['undetermined'] += 1
                else:
                    if band not in self.stats['bands']:
                        self.stats['bands'][band] = 0
                    self.stats['bands'][band] += 1
                qso['mode'] = line.split()[2].upper()  # line[11:13].strip().upper()
                if not is_allowed_mode(qso['mode']):
                    reason = 'Unsupported mode "{mode}"'.format(mode=qso['mode'])
                    logger.warning('Cabrillo.qsos: {reason}: {line}'.format(reason=reason, line=line))
                    self.stats['invalid'].append('{reason}: {line}'.format(reason=reason, line=line))
                    qso['f_invalidation_reason'] += reason
                    qso['valid'] = False
                elif (self.category_mode == 'CW' and not qso['mode'].upper() == 'CW') or \
                        (self.category_mode == 'SSB' and not qso['mode'].upper() == 'PH'):
                    reason = 'Unsupported mode "{mode}" for chosen category'.format(mode=qso['mode'])
                    logger.warning('Cabrillo.qsos: {reason}: {line}'.format(reason=reason, line=line))
                    self.stats['invalid'].append('{reason}: {line}'.format(reason=reason, line=line))
                    qso['f_invalidation_reason'] += reason
                    qso['valid'] = False
                if not qso['mode'].upper() in self.stats['modes']:
                    self.stats['modes'][qso['mode'].upper()] = 0
                self.stats['modes'][qso['mode'].upper()] += 1
                date_string = time_string = format_string = ''
                try:
                    time_string = line.split()[4]
                    if int(time_string) < 1000 and not str(time_string).startswith('0'):
                        # time string is not 0 padded
                        time_string = '0{}'.format(time_string)
                    date_string = line.split()[3]
                    if not date_string.startswith(str(datetime.now().year)) and date_string.endswith(str(datetime.now().year)[2:]):
                        # dutchies writing differently formatted date
                        date_string = '{}-{}-20{}'.format(date_string.split('-')[0], date_string.split('-')[1], date_string.split('-')[2])
                        format_string = '%d-%m-%Y %H%M'
                    else:
                        format_string = '%Y-%m-%d %H%M'
                    qso['datetime'] = datetime.strptime('{a} {b}'.format(a=date_string,  # line[14:27],
                                                                         b=time_string),  # line[27:29]),
                                                        format_string)
                    if not verify_time(qso['datetime'], self.contest):
                        reason = 'Log time "{datetime}" is outside of contest running time'.format(
                            datetime=qso['datetime'])
                        logger.warning('Cabrillo.qsos: {reason}: {line}'.format(reason=reason, line=line))
                        self.stats['invalid'].append('{reason}: {line}'.format(reason=reason, line=line))
                        qso['f_invalidation_reason'] += reason
                        qso['valid'] = False
                except ValueError:
                    reason = 'Unable to parse date "{date}" and time "{time}" using format "{format}"'.format(
                        date=date_string, time=time_string, format=format_string
                    )
                    logger.warning('Cabrillo.qsos: {reason}: {line}'.format(reason=reason, line=line))
                    self.stats['invalid'].append('{reason}: {line}'.format(reason=reason, line=line))
                    qso['f_invalidation_reason'] += reason
                    qso['valid'] = False
                    qso['datetime'] = datetime(1970, 1, 1)
                qso['callsign'] = line.split()[8]  # line[55:68].strip()
                if not self.DxCC.get_country(qso['callsign']):
                    self.stats['warnings'].append('Could not get country for call {} in line: {}'.format(qso['callsign'],
                                                                                                     line))
                qso['exchange'] = ' '.join(line.split()[5:-1])  # line[30:-1].strip()
                if len(line.split()) == 12:
                    qso['transmitterID'] = line.split()[11]
                else:
                    qso['transmitterID'] = -1
                qso['infoSent'] = ' '.join(line.split()[6:8])  # line[44:54].strip()
                qso['infoReceived'] = ' '.join(line.split()[9:11])  # line[69:79].strip()
                qso['valid'] = valid
                logger.debug('Cabrillo.qsos: yielding qso: {}'.format(qso))
                if valid:
                    self.stats['valid'] += 1
                yield (qso)
