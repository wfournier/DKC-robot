import os
import requests
import logging
logger = logging.getLogger('dkc.dxcc')


class DXCC(object):
    """
    Describes the DXCC list
    """
    def __init__(self):
        logger.debug('DXCC.__init__()')
        self.__dxcc_list = {}
        self.__download_cty_file()
        self.__create_database()
        pass

    def __download_cty_file(self):
        """
        Download the list
        """
        # https://github.com/hsmade/DKC-robot/issues/1
        logger.debug('DXCC.__downloadXTY()')
        #download = requests.get('http://www.country-files.com/cty/cty.dat')
        #download = requests.get('http://downloads.dkars.nl/cty.dat')
        #if not download.status_code == 200:
            #raise Exception('Error downloading cty.dat: '+download.text)
        self.cty = open(os.path.join(os.path.dirname(os.path.realpath(__file__)), '..', 'databases', 'cty.dat')).read()
        #self.cty = download.text

    def __create_database(self):
        """
        Parse the list
        """
        logger.debug('DXCC.__createDB()')
        self.dxccList = {}
        country = False
        for line in self.cty.split('\n'):
            line = line.strip()
            if line.endswith(':'):
                # definition line
                country = line[:26].strip()
                country = country[:len(country)-1]
            else:
                line = line.replace(';', '')
                #line = line.replace('&','')
                # prefixes
                for call in line.split(','):
                    call = call.replace('=', '')
                    if '[' in call:
                        call = call[:call.index('[')]
                    if '(' in call:
                        call = call[:call.index('(')]
                    self.dxccList[call] = country

    def get_country(self, call):
        """
        Get a country code looking up the call in the list
        :return:
        The country as string or False
        """
        logger.debug('DXCC.getCountry("{}")'.format(call))
        call = call.strip().upper()
        size = len(call) - 1
        dxcc = False
        while size > 0:
            if call[:size].upper() in self.dxccList:
                dxcc = self.dxccList[call[:size]]
                size = 0
            else:
                size -= 1
        if not dxcc:
            logger.warning('DXCC.getCountry: no dxcc found for {}'.format(call))
        logger.debug('DXCC.getCountry: found dxcc {} for call {}'.format(dxcc, call))
        return dxcc
