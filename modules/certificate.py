import ezodf
from tempfile import mkstemp
from subprocess import call
import os


class Certificate(object):
    def __init__(self, template):
        self.doc = ezodf.opendoc(template)

    def create(self, place, callsign, category, points):
        new_doc = self.doc
        xml = new_doc.content.xmlnode
        self.find_text(xml, 'SCORE_PLACE').text = place + ' place'
        self.find_text(xml, 'SCORE_CALLSIGN').text = callsign
        self.find_text(xml, 'SCORE_CATEGORY').text = category
        self.find_text(xml, 'SCORE_POINTS').text = str(points)
        _, tempfile_base = mkstemp()
        os.remove(tempfile_base)
        new_doc.saveas(tempfile_base + '.odp')
        result = call([
            'soffice',
            '--headless',
            '--convert-to',
            'pdf',
            '--outdir',
            os.path.dirname(tempfile_base),
            tempfile_base + '.odp'
        ])
        if not result == 0:
            raise RuntimeError('Conversion failed')
        os.remove(tempfile_base + '.odp')
        return tempfile_base + '.pdf'

    def find_text(self, root, text, depth=0):
        for elm in root.getchildren():
            # print ' ' * depth + str(elm), elm.text
            if elm.text is not None and text in elm.text:
                print 'FOUND', elm
                return elm
            elif elm.getchildren():
                result = self.find_text(elm, text, depth + 1)
                if result is not None:
                    return result
            else:
                continue
        return None
