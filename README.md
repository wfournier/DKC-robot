DKC-robot
=========

This is the Dutch Kingdom Contest Robot. It's implemented using web2py. It allows radio amateurs to send in their logs (Cabrillo format) and does the scoring.
For more info, check the site: http://dkars.nl/index.php?page=contest_uk2
