# -*- coding: utf-8 -*-
import logging
import datetime
from tempfile import mkstemp
from certificate import Certificate

logger = logging.getLogger('dkc.controller')

def index():
    # https://github.com/hsmade/DKC-robot/issues/8
    contests = db().select(db.t_contests.ALL)
    now = datetime.datetime.now()
    closed_contest = False
    in_contest = False
    for contest in contests:
        if contest['f_start'] <= now <= contest['f_closed']:
            closed_contest = contest
        elif contest['f_start'] <= now >= contest['f_closed']:
            in_contest = contest
    if closed_contest:
        return redirect(URL('upload'))
    else:
        if in_contest and in_contest['f_published']:
            return redirect(URL('scores', args=(in_contest['id'])))
        elif in_contest and in_contest['f_published']:
            return dict(status='not_published')
        else:
            return dict(status='no_contest')


def validate_upload(form):
    if form.vars.f_category_region == '-':
        form.errors.f_category_region = T('Please select a value from this list')
    if form.vars.f_category_mode == '-':
        form.errors.f_category_mode = T('Please select a value from this list')
    if form.vars.f_category_power == '-':
        form.errors.f_category_power = T('Please select a value from this list')


def upload():
    """
    Handles the uploading of a log file
    """
    logger.debug('default.upload()')
    contests = db().select(db.t_contests.ALL)
    now = datetime.datetime.now()
    in_contest = False
    form = ''
    for contest in contests:
        if contest['f_start'] <= now <= contest['f_closed']:
            in_contest = contest['id']
    if in_contest:
        form = SQLFORM(db.t_uploads, formstyle='divs')
        # give the input a bit larger field size
        form.element('select', _name='f_category_power')['_style'] = 'width:500px'
        # if form.process().accepted:
        if form.accepts(request.vars, session, onvalidation=validate_upload):
            logger.debug('default.upload: uploaded file')
            logger.debug('default.upload: form: {}'.format(form))
            upload_id = form.vars.id
            logger.debug('default.upload: upload stored with ID {}'.format(upload_id))
            scheduler.queue_task(upload_log, pvars=dict(upload_id=upload_id,
                                                        contest_id=in_contest
                                                        ), timeout=300)
            response.flash = T('Your log was successfully uploaded and will now be processed by the system. \
                Once processed, you will receive a mail with the parsing results.')
        elif form.errors:
            response.flash = T('Please check the form for errors')
    return dict(form=form)


@auth.requires_login()
def expats():
    """
    Handles management of expats
    """
    grid = SQLFORM.grid(db.t_callsign, user_signature=True)
    return dict(grid=grid)


@auth.requires_login()
def uploads():
    """
    Handles management of uploads
    """
    # grid = SQLFORM.grid(db.t_uploads, user_signature=True, editable=False, deletable=False, create=False)
    grid = SQLFORM.grid(db.t_uploads, db.t_logs, left=db.t_logs.on(db.t_uploads.id == db.t_logs.f_upload_ID),
                        user_signature=True, editable=False, deletable=False, create=False)
    return dict(grid=grid)


@auth.requires_login()
def score():
    form = SQLFORM.factory(Field('contest', requires=IS_IN_DB(db, 't_contests.id',
                                                              't_contests.f_description',
                                                              zero=None),
                                 label=T('Select contest')))
    if form.process().accepted:
        logger.debug('controller.score: do scoring for ID {}'.format(form.vars.contest))
        response.flash = T('Calculating scores')
        do_scoring(form.vars.contest)
        redirect(URL('scores'))
    return dict(form=form)


def certificate():
    """Generate a certificate"""
    if not len(request.args) == 2:
        raise HTTP(404)

    callsign = request.args[0].replace('_', '/')
    score = db(
        (db.t_scores.f_callsign == callsign) &
        (db.t_scores.f_contest == request.args[1])
    ).select().first()

    if not score:
        raise HTTP(404)

    scores_in_category = [row for row in db(
        (db.t_scores.f_category == score.f_category) &
        (db.t_scores.f_contest == score.f_contest)
    ).select(orderby=~db.t_scores.f_total_score)]

    ordinal = lambda n: "%d%s" % (n, "tsnrhtdd"[(n / 10 % 10 != 1) * (n % 10 < 4) * n % 10::4])
    place = ordinal(scores_in_category.index(score) + 1)
    cert = Certificate(os.path.join(request.folder, 'empty.odp'))
    certificate_file = cert.create(place, score.f_callsign, score.f_category, score.f_total_points)
    certificate_content = open(certificate_file).read()
    os.remove(certificate_file)
    response.headers['Content-Type'] = 'application/vnd.oasis.opendocument.presentation'
    response.headers['Content-Disposition'] = 'attachment; filename="{call}.pdf"'.format(call=callsign)
    return certificate_content


def scores():
    """
    Temporary view to show scoring
    """
    scores = ''
    manage_contest = ''
    categories = ''

    if len(request.args) > 0:
        contest = request.args[0]
    else:
        logger.debug('scores: no contest id')
        contest = False
    contest_form = SQLFORM.factory(Field('contest',
                                         requires=IS_IN_DB(db, 't_contests.id',
                                                           't_contests.f_description',
                                                           zero=None),
                                         label=T('Select contest')))
    if contest_form.process().accepted:
        contest = contest_form.vars.contest
    if contest:
        try:
            logger.debug('scores: looking up contest with ID: %s', contest)
            contest_db = db(db.t_contests.id == contest).select()[0]
        except IndexError:
            pass
        else:
            if contest_db['t_contests.f_published'] or (not contest_db['t_contests.f_published'] and auth.user):
                # scores_db = db(db.t_scores.f_contest == contest_form.vars.contest).select(db.t_scores.ALL)
                query = ((db.t_scores.f_contest == contest) &
                         (db.t_logs.f_operator == db.t_scores.f_callsign) &
                         (db.t_logs.f_contest == db.t_scores.f_contest))
                scores_db = db(query).select().sort(lambda r: ~r.t_scores.f_total_score)
                categories = db().select(db.t_categories.ALL, orderby=db.t_categories.f_name)

                category_list = [category['t_categories.f_name'] for category in categories]
                scores = {}
                for row in scores_db:
                    if row['t_logs.f_category'] in category_list:
                        if not row['t_logs.f_category'] in scores:
                            scores[row['t_logs.f_category']] = []
                        scores[row['t_logs.f_category']].append(row)
                logger.debug('scores_db: {}'.format(scores_db))
                if auth.user:
                    manage_contest = '{}/{}'.format(URL('contests'), contest)
            else:
                contest_form = T('Not published yet')
    return dict(contest=contest_form, manage=manage_contest, scores=scores, categories=categories, contest_id=contest, logged_in=(
    auth.user is not None))


@auth.requires_login()
def contests():
    """
    Manage contests and their bands
    """
    contest_select_form = ''
    contest_form = ''
    if len(request.args) > 0:
        contest = request.args[0]
    else:
        contest = False
        contest_select_form = SQLFORM.factory(Field('contest',
                                                    requires=IS_IN_DB(db, 't_contests.id',
                                                                      't_contests.f_description',
                                                                      zero=None),
                                                    label=T('Select contest')))
        if contest_select_form.process().accepted:
            contest = contest_select_form.vars.contest
            logger.debug('Redirect to {}'.format(contest))
            redirect(URL('contests', args=(contest)))
    if contest:
        logger.debug('Got contest: {}'.format(contest))
        contest_form = SQLFORM(db.t_contests)
        try:
            contest_row = db(db.t_contests.id == contest).select().first()
            for key in contest_row.keys():
                if key.startswith('f_'):
                    setattr(contest_form.vars, key, getattr(contest_row, key))
        except Exception as e:
            logger.error(e)
            return False

        bands = db().select(db.t_bands.ALL)
        bands_for_contest = [row.f_band for row in db(db.t_bands_for_contest.f_contest == contest).select()]
        for band in bands:
            contest_form[0].insert(-1, TR(LABEL(band.f_name),
                                          INPUT(_name='band_{}'.format(band.f_name),
                                                value=(band.id in bands_for_contest), _type='checkbox')
                                          ))
        if contest_form.validate():
            logger.debug('Saving changes to contest_form: {}'.format(contest_form.vars))
            for key in contest_form.vars.keys():
                if key.startswith('f_'):
                    setattr(contest_row, key, getattr(contest_form.vars, key))
            logger.debug('contest_row is now: {}'.format(contest_row))
            contest_row.update_record()
            for band in bands:
                if getattr(contest_form.vars, 'band_{}'.format(band.f_name)):
                    if band.id not in bands_for_contest:
                        logger.debug('Adding band {} to contest {}'.format(band.id, contest))
                        db.t_bands_for_contest.insert(f_contest=contest, f_band=band.id)
                else:
                    if band.id in bands_for_contest:
                        logger.debug('Removing band {} to contest {}'.format(band.id, contest))
                        db(db.t_bands_for_contest.f_contest and
                           db.t_bands_for_contest.f_band == band.id).delete()
            redirect(URL('contests', args=(contest)))
    return locals()


@auth.requires_login()
def log_detail():
    grid = SQLFORM.smartgrid(db.t_logs, linked_tables=['t_qsos'],
                             constraints={'t_logs': (db.t_logs.f_operator == request.vars.callsign) &
                                                    (db.t_logs.f_contest == request.vars.contest)},
                             user_signature=False)
    return locals()


@auth.requires_login()
def soapbox():
    soapboxes = []

    if len(request.args) > 0:
        contest = request.args[0]
    else:
        contest = False
    contest_form = SQLFORM.factory(Field('contest',
                                         requires=IS_IN_DB(db, 't_contests.id',
                                                           't_contests.f_description',
                                                           zero=None),
                                         label=T('Select contest')))
    if contest_form.process().accepted:
        contest = contest_form.vars.contest
    if contest:
        try:
            contest_db = db(db.t_contests.id == contest).select()[0]
        except IndexError:
            pass
        else:
            query = ((db.t_logs.f_contest == contest) &
                     (db.t_logs.f_soapbox != ''))
            soapboxes = db(query).select()

    return dict(contest=contest_form, soapboxes=soapboxes, contest_id=contest)


def user():
    """
    exposes:
    http://..../[app]/default/user/login
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    http://..../[app]/default/user/manage_users (requires membership in
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    """
    return dict(form=auth())
