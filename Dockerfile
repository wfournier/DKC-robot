FROM ubuntu
RUN apt-get update; apt-get install -y apache2 python-setuptools unzip git libapache2-mod-wsgi
RUN easy_install pip; pip install requests
ADD http://www.web2py.com/examples/static/web2py_src.zip /web2py.zip
RUN mkdir -p /opt/DKC-robot /opt/DKC-robot/htdocs /var/log/apache/dkars.nl/
WORKDIR /opt/DKC-robot
RUN unzip /web2py.zip; rm /web2py.zip; cp web2py/handlers/wsgihandler.py web2py/; chown -R www-data web2py
COPY contest.dkars.nl.conf /etc/apache2/sites-available/
RUN a2ensite contest.dkars.nl.conf
WORKDIR /opt/DKC-robot/web2py/applications
RUN git clone https://github.com/hsmade/DKC-robot.git dkcrobot; chown www-data dkcrobot
RUN ln -s /opt/DKC-robot/web2py/applications/dkcrobot/routes.py /opt/DKC-robot/web2py/; ln -s /opt/DKC-robot/web2py/applications/dkcrobot/logging.json /opt/DKC-robot/web2py/
RUN apache2ctl configtest
ENTRYPOINT ["/usr/sbin/apache2ctl","-D","FOREGROUND"]
