from gluon.scheduler import Scheduler
import logging

logger = logging.getLogger('dkc.scheduler')
logging.basicConfig(level=logging.DEBUG)
from dxcc import DXCC
from cabrillo import Cabrillo

scheduler_db = DAL('sqlite://installer_scheduler.sqlite')
scheduler = Scheduler(scheduler_db)
from gluon import current

db = current.db


# TODO move the helper functions to their own class
# originals in cabrillo.py
# https://github.com/hsmade/DKC-robot/issues/2
def get_band(freq, contest):
    if freq < 1000:
        freq *= 1000
    for band in db((db.t_bands_for_contest.f_contest == contest) &
                           (db.t_bands.id == db.t_bands_for_contest.f_band)).select(db.t_bands.ALL):
        if band['f_low'] <= freq <= band['f_high']:
            return band['f_name']
    return False


def is_expat(callsign):
    """
    Checks if the given call is an expat
    :param callsign:
    :return: boolean
    """
    result = db((db.t_callsign.f_callsign.upper() == callsign.upper()) &
                (db.t_callsign.f_expat_status is True)).select()
    return len(result) > 0


def find_partner_qso(lookfor):
    """
    finds the related log entry in the QSO partners log and marks it
    :return:
    The database ID that was found or 0
    """
    logger.debug('scheduler.findPartnerQSO()')
    database_id = 0
    result = db((db.t_qsos.f_callsign == lookfor.get('fromCallsign')) &
                (db.t_qsos.f_from_callsign == lookfor.get('callsign')) &
                (db.t_qsos.f_freq == lookfor.get('freq')) &
                (db.t_qsos.f_mode == lookfor.get('mode')) &
                (db.t_qsos.f_info_received == lookfor.get('infoSent')) &
                (db.t_qsos.f_info_sent == lookfor.get('infoReceived'))
                ).select(db.t_qsos.id, db.t_qsos.f_datetime)
    if len(result) > 0:
        try:
            diff = (result[0]['f_datetime'] - lookfor['datetime']).total_seconds()
        except Exception as error:
            logger.warn('scheduler.findPartnerQSO: error diffing dates {} and {}'.format(result[0]['f_datetime'],
                                                                                         lookfor['datetime']))
            diff = 65535
        logger.debug('scheduler.findPartnerQSO: Found matching record with id {id} and time diff {diff}s'.format(
            id=result[0]['id'],
            diff=diff
        ))
        # time difference may be up to 5 minutes according to DKARS rules
        if diff < 300.0:
            database_id = result[0]['id']
    return database_id


def send_upload_result_email(email, success=True, reason="", error="", stats=None):
    """
    Sends an e-mail with the parser result to the submitter
    """
    if not stats:
        stats = {}
    logger.debug('scheduler.sendEmail()')
    # TODO: remove, for debugging
    # email = 'ph7wim@dkars.nl'
    if not success:
        logger.debug('scheduler.sendEmail: success=False')
        subject = '[DKC contest robot] Errors during log parsing'
        text = '''This is to inform you that the contest robot found errors while parsing the log that you've submitted.
                  Please find the errors below.'''
    else:
        logger.debug('scheduler.sendEmail: success=True')
        subject = '[DKC contest robot] Log parsing successful'
        text = '''The contest robot has successfully parsed your log. Please find some statistics below.'''
    all_errors = list()
    if error:
        logger.debug('scheduler.sendEmail: have error')
        all_errors.append('{}: {}'.format(reason, error))
    if 'invalid' in stats and stats['invalid']:
        logger.debug('scheduler.sendEmail: have stats[invalid]')
        all_errors.extend(stats['invalid'])
    if 'total' not in stats:
        stats['total'] = 0

    message_text = '''
{text}

General statistics:
===================
Total QSO lines parsed: {total}
Valid QSOs: {valid}

Bands used:
===========
  {bands}

Modes used:
===========
  {modes}
    '''.format(
        text=text,
        total=stats['total'],
        valid=stats.get('valid', 'none'),
        bands='\n  '.join(['{}: {}'.format(band, total) for band, total in stats.get('bands', {}).iteritems()]),
        modes='\n  '.join(['{}: {}'.format(mode, total) for mode, total in stats.get('modes', {}).iteritems()]).replace(
            'PH', 'Phone')
    )

    if all_errors:
        message_text += '\n\nErrors ({num_errors}):\n{errors}'.format(
            num_errors=len(all_errors),
            errors='\n'.join(all_errors))

    if getattr(stats, 'warnings', False):
        message_text += '\n\nWarnings ({num_warnings}):\n{warnings}'.format(
            num_warnings=len(stats['warnings']),
            warnings='\n'.join(stats['warnings'])
        )
    logger.debug(
        'scheduler.sendEmail: sending mail to {mail} with subject: {subject} and text: {text}'.format(subject=subject,
                                                                                                      text=message_text,
                                                                                                      mail=email))
    try:
        mail.send(email, subject, message_text, bcc='ph7wim@dkars.nl')
    except Exception as e:
        logger.error('scheduler.sendEmail: Failed sending email: {}'.format(e))
        raise Exception(e)


def upload_log(upload_id, contest_id):
    """
    scheduler to upload a log into the DB. Called by the upload controller
    """
    logger.info('scheduler.uploadLog({},{})'.format(upload_id, contest_id))
    contest = db(db.t_contests.id == contest_id).select(db.t_contests.ALL).first()
    logger.debug('upload_log: matched contest: {}'.format(contest))
    records = db(db.t_uploads.id == upload_id).select()
    if len(records) != 1:
        logger.error('upload_log: tried to fetch log with id {} and got {} rows'.format(upload_id, len(records)))
        return False
    upload_record = records.first()
    email = upload_record.get('t_uploads.f_email', '')
    category_record = db(db.t_categories.f_name == '{}{}{}'.format(
        upload_record['t_uploads.f_category_region'],
        upload_record['t_uploads.f_category_power'],
        upload_record['t_uploads.f_category_mode'])
                         ).select().first()
    if category_record and 'f_name' in category_record:
        category = category_record['f_name']
    else:
        logger.error('Failed matching category ID {} to category. This should not happen'.format(
            upload_record['t_uploads.f_category']))
        send_upload_result_email(email, success=False, reason='Failed parsing category',
                                 error='Please contact contest@dkars.nl with a copy of this mail and your log')
        raise RuntimeError('Stopped processing uploaded log due to mismatch in category. ID: {}'.format(
            upload_record['t_uploads.f_category']))
    logger.debug('scheduler.uploadLog: found email: {} and cateogry: {}'.format(email, category))
    logger.debug('scheduler.uploadLog: creating Cabrillo object')
    try:
        log = Cabrillo(db.t_uploads.f_file.retrieve(upload_record['t_uploads.f_file'])[1], contest_id,
                       upload_record['t_uploads.f_category_mode'])
    except Exception as e:
        logger.error('scheduler.upload_log: Error while parsing log: {}'.format(e))
        send_upload_result_email(email, success=False, reason="Failed parsing Cabrillo header", error=e.message)
        raise Exception(e)
    dxcc = DXCC()
    logger.debug('scheduler.uploadLog: parsing header')
    try:
        operator, country, soapbox, claimed_score = log.parse_header
    except Exception as e:
        send_upload_result_email(email, success=False, reason="Failed parsing Cabrillo header", error=e.message)
        raise Exception(e)
    query = ((db.t_logs.f_operator == operator.upper()) & (db.t_logs.f_contest == contest['id']))
    logger.debug('scheduler.uploadLog: query for existing header: {}'.format(query))
    logs_row = db(query).select(db.t_logs.ALL).first()
    if logs_row:
        logger.debug('scheduler.uploadLog: found an existing record for {}: {}, updating'.format(
            operator.upper(),
            logs_row['id']
        ))
        logs_row.update_record(f_operator=operator.upper(),
                               f_category=category.upper(),
                               f_country=country.upper(),
                               f_soapbox=soapbox,
                               f_email=email,
                               f_claimed_score=claimed_score,
                               f_upload_ID=upload_id,
                               f_contest=contest['id'],
                               f_swl=upload_record['t_uploads.f_swl'],
                               )
        log_id = logs_row['id']
    else:
        logger.debug('scheduler.uploadLog: inserting record for {}'.format(operator.upper()))
        log_id = db.t_logs.insert(f_operator=operator.upper(),
                                  f_category=category.upper(),
                                  f_country=country.upper(),
                                  f_soapbox=soapbox,
                                  f_email=email,
                                  f_claimed_score=claimed_score,
                                  f_upload_ID=upload_id,
                                  f_contest=contest['id'],
                                  f_swl=upload_record['t_uploads.f_swl'],
                                  )
        logs_row = db(db.t_logs.id == log_id).select(db.t_logs.ALL).first()

    logger.debug('scheduler.uploadLog: loop over qsos')
    for qso in log.qsos():
        logger.debug('scheduler.uploadLog: finding partner for {}'.format(qso))
        from_id = find_partner_qso(qso)
        qso_row = db(db.t_qsos.f_qsoline == qso['qsoLine']).select(db.t_qsos.ALL).first()
        if not qso_row:
            logger.debug('scheduler.uploadLog: inserting qso record for {}'.format(qso['fromCallsign'].upper()))
            qso_id = db.t_qsos.insert(
                f_log=logs_row,
                f_contest=contest['id'],
                f_from_callsign=qso['fromCallsign'].upper(),
                f_from_id=from_id,
                f_qsoline=qso['qsoLine'],
                f_freq=qso['freq'],
                f_mode=qso['mode'].upper(),
                f_datetime=qso['datetime'],
                f_callsign=qso['callsign'].upper(),
                f_exchange=qso['exchange'].upper(),
                f_transmitter_id=qso['transmitterID'],
                f_info_sent=qso['infoSent'].upper(),
                f_info_received=qso['infoReceived'].upper(),
                f_country=dxcc.get_country(qso['callsign']),
                f_valid=qso['valid'],
                f_invalidation_reason=qso['f_invalidation_reason'],
            )
        else:
            qso_id = qso_row['id']
            logger.debug('scheduler.uploadLog: updating record for {} with id {} (f_invalidation_reason={})'.format(
                qso['fromCallsign'].upper(),
                qso_id,
                qso['f_invalidation_reason']
            ))
            qso_row.update_record(
                f_log=logs_row,  # TODO: should we update this?
                f_contest=contest['id'],
                f_from_callsign=qso['fromCallsign'].upper(),
                f_from_id=from_id,
                f_qsoline=qso['qsoLine'],
                f_freq=qso['freq'],
                f_mode=qso['mode'].upper(),
                f_datetime=qso['datetime'],
                f_callsign=qso['callsign'].upper(),
                f_exchange=qso['exchange'].upper(),
                f_transmitter_id=qso['transmitterID'],
                f_info_sent=qso['infoSent'].upper(),
                f_info_received=qso['infoReceived'].upper(),
                f_country=dxcc.get_country(qso['callsign']),
                f_valid=qso['valid'],
                f_invalidation_reason=qso['f_invalidation_reason'],
            )
        rows = db(db.t_qsos.id == from_id).select()
        logger.debug('scheduler.uploadLog: Found {} rows that partner with this QSO'.format(len(rows)))
        if rows:
            row = rows.first()  # should not be possible to get more records, but just in case..
            logger.debug(
                'scheduler.uploadLog: updating id: {id} to have f_from_id {new}'.format(id=from_id, new=qso_id))
            row.update_record(f_from_id=qso_id)
    upload_record.update_record(f_processed=True)

    if upload_record['t_uploads.f_expat_motivation']:
        expat = db(db.t_callsign.f_callsign == operator).select().first()
        if not expat:
            db.t_callsign.insert(f_callsign=operator,
                                 f_expat_status=True,
                                 f_motivation=upload_record['t_uploads.f_expat_motivation'])
        elif not expat['t_callsign.f_expat_status']:
            logger.info('Updated expat status to True for callsign {}'.format(operator))
            expat.update_record(f_expat_status=True, f_motivation=upload_record['t_uploads.f_expat_motivation'])
            mail.send(admin_email, 'Changed expat status for {}'.format(operator),
                      """
This is to inform you that someone with callsign {operator} motivated his/her expat status with:

{motivation}

Their expat status was set.
If you don't think this is correct, manage the record here:
https://contest.dkars.nl/dkcrobot/expats
                      """.format(operator=operator,
                                 motivation=upload_record['t_uploads.f_expat_motivation']))
    db.commit()
    send_upload_result_email(email, success=True, stats=log.stats)


def do_scoring(contest_id, log_id=False):
    """
    This function does the scoring for one or all IDs with the given contestID
    """
    logger.debug('scheduler.doScoring()')
    contest = db(db.t_contests.id == contest_id).select(db.t_contests.ALL).first()

    def qso_has_doubles(match_qso):
        """
        Finds qsos with the same opponent on the same band with the same mode
        These will not be counted as valid qso
        :return:
        The rows found or False
        """
        doubles_query = ((db.t_qsos.f_contest == match_qso['f_contest']) &
                         (db.t_qsos.f_callsign == match_qso['f_callsign']) &
                         (get_band(db.t_qsos.f_freq, contest['id']) == get_band(match_qso['f_freq'], contest['id'])) &
                         (db.t_qsos.f_from_callsign == match_qso['f_from_callsign']) &
                         (db.t_qsos.f_mode == match_qso['f_mode'])
                         )
        return db(doubles_query).select(db.t_qsos.ALL) or False

    if log_id:
        logs_query = (db.t_logs.id == log_id) & (db.t_logs.f_contest == contest['id'])
    else:
        logs_query = db.t_logs.f_contest == contest['id']

    dxcc = DXCC()

    logger.debug('scheduler.doScoring: query: {}'.format(logs_query))
    logs = db(logs_query).select(db.t_logs.ALL)
    logger.debug('scheduler.doScoring: found {} log rows'.format(len(logs)))
    for log in logs:
        logger.debug('scheduler.doScoring: working on callsign: {}'.format(log['f_operator']))
        scoring_dict = {
            'f_contest': contest['id'],
            'f_callsign': log['f_operator'],
            'f_category': log['f_category'],
            'f_total': 0,
            'f_skipped': 0,
            'f_points': 0,
            'f_multiplier': 0,
            'f_total_score': 0,
        }

        processed_sequence_numbers = []
        processed_calls_per_band = []
        processed_dxccs = []
        is_swl = log['f_swl']
        processed_swl_callsigns = {}

        logger.debug('scheduler.doScoring: looking for QSOs')
        # Don't match up with operator but log ID instead
        qsos_query = ((db.t_qsos.f_contest == contest['id']) & (db.t_qsos.f_log == log))
        qsos = db(qsos_query).select(db.t_qsos.ALL)
        logger.debug('scheduler.doScoring: found {} qso rows'.format(len(qsos)))
        for qso in qsos:
            scoring_dict['f_total'] += 1  # count each qso
            band = get_band(qso['f_freq'], contest['id'])

            # create 'hash' for the sequence number
            if log['f_category'][1] == '4':  # multi station
                sequence_hash = ' '.join(qso['f_info_sent'].split()) + str(band)  # multi station is per band
            else:
                sequence_hash = ' '.join(qso['f_info_sent'].split())

            if is_swl:
                # update stats on callsigns seen
                processed_swl_callsigns[qso['f_callsign']] = processed_swl_callsigns.get(qso['f_callsign'], 0) + 1
                processed_swl_callsigns[qso['f_from_callsign']] = processed_swl_callsigns.get(
                    qso['f_from_callsign'], 0) + 1

            call_band_hash = '{} {}'.format(qso['f_callsign'], band)

            if not qso['f_valid']:
                reason = 'Skipping QSO marked as invalid for id: {}'.format(qso['id'])
                logger.debug(reason)
                # qso.update_record(f_invalidation_reason=reason)  # already has a reason
                scoring_dict['f_skipped'] += 1
            elif not band:
                reason = 'Skipping QSO as it has no valid band. ID: {}. freq:{}'.format(
                    qso['id'], qso['f_freq'])
                logger.debug(reason)
                qso.update_record(f_invalidation_reason=reason)
                scoring_dict['f_skipped'] += 1
            elif not is_swl and sequence_hash in processed_sequence_numbers:
                reason = 'Skipping RST we\'ve already seen for id: {}'.format(qso['id'])
                logger.debug(reason)
                qso.update_record(f_invalidation_reason=reason)
                scoring_dict['f_skipped'] += 1
            # https://github.com/hsmade/DKC-robot/issues/14
            # elif not swl and not qso['f_from_id'] or qso['f_from_id'] == 0:
            # logger.debug('scheduler.doScoring: unmatched QSO for id: {}'.format(qso['id']))
            # scoring['f_skipped'] += 1
            # check on the Dict first so we don't always do a DB lookup
            elif not is_swl and call_band_hash in processed_calls_per_band and qso_has_doubles(qso):
                reason = 'Skipping call + band we\'ve already seen for id: {}'.format(qso['id'])
                logger.debug(reason)
                qso.update_record(f_invalidation_reason=reason)
                scoring_dict['f_skipped'] += 1
            elif is_swl and (not qso['f_from_callsign'] or not qso['f_callsign']):
                reason = 'Skipping swl record as it misses a call sign. id:'.format(qso['id'])
                logger.debug(reason)
                qso.update_record(f_invalidation_reason=reason)
                scoring_dict['f_skipped'] += 1
            elif is_swl and processed_swl_callsigns.get(qso['f_callsign'], 0) > 5 or \
                    processed_swl_callsigns.get(qso['f_from_callsign'], 0) > 5:
                reason = 'Skipping swl record as we\'ve seen a callsign over 5 times. id:'.format(
                        qso['id'])
                logger.debug(reason)
                qso.update_record(f_invalidation_reason=reason)
                scoring_dict['f_skipped'] += 1
            else:  # no issues left, so score this qso
                # if is_swl:
                #     scoring_dict['f_points'] += 1
                #     points = 1
                #     continue
                # https://github.com/hsmade/DKC-robot/issues/11
                processed_sequence_numbers.append(sequence_hash)
                processed_calls_per_band.append(call_band_hash)
                # https://github.com/hsmade/DKC-robot/issues/5
                dxcc_band_mode_hash = '{}{}{}'.format(get_band(qso['f_freq'], contest['id']),
                                                 dxcc.get_country(qso['f_callsign']),
                                                 qso['f_mode'].upper())
                if not is_swl and dxcc_band_mode_hash not in processed_dxccs:  # new DXCC / not swl
                    if qso['f_callsign'][0:3].upper() in ('PA', 'PB', 'PC', 'PD', 'PE', 'PF', 'PG', 'PH', 'PI', 'P4'):
                        scoring_dict['f_multiplier'] += 10
                        if qso['f_callsign'].upper().replace(' ', '').endswith('/P'):
                            scoring_dict['f_points'] += 10
                        else:
                            scoring_dict['f_points'] += 5
                    elif qso['f_callsign'][0:4].upper() in ('PJ2', 'PJ4', 'PJ5', 'PJ6', 'PJ7'):
                        scoring_dict['f_multiplier'] += 10
                        if qso['f_callsign'].upper().replace(' ', '').endswith('/P'):
                            scoring_dict['f_points'] += 10
                        else:
                            scoring_dict['f_points'] += 5
                    elif is_expat(qso['f_callsign']):
                        scoring_dict['f_points'] += 5
                    else:
                        scoring_dict['f_multiplier'] += 5
                        scoring_dict['f_points'] += 1
                    processed_dxccs.append(dxcc_band_mode_hash)
                else:  # also swl
                    scoring_dict['f_points'] += 1
        scoring_dict['f_total_score'] = scoring_dict['f_points'] * scoring_dict['f_multiplier']

        score_row = db(
            (db.t_scores.f_contest == contest['id']) &
            (db.t_scores.f_callsign == log['f_operator'])
        ).select(db.t_scores.ALL).first()

        if score_row:  # update existing score row
            logger.debug('scheduler.doScoring: updating score: {}'.format(scoring_dict))
            score_row.update(scoring_dict)
            score_row.update_record()
        else:  # create new score row
            logger.debug('scheduler.doScoring: inserting score: {}'.format(scoring_dict))
            db.t_scores.insert(**scoring_dict)
        db.commit()
