# -*- coding: utf-8 -*-
from datetime import datetime
from gluon import current
import os.path

## app configuration made easy. Look inside private/appconfig.ini
from gluon.contrib.appconfig import AppConfig
## once in production, remove reload=True to gain full speed
myconf = AppConfig(reload=True)

#########################################################################
## This scaffolding model makes your app work on Google App Engine too
## File is released under public domain and you can use without limitations
#########################################################################

## if SSL/HTTPS is properly configured and you want all HTTP requests to
## be redirected to HTTPS, uncomment the line below:
# request.requires_https()

if not request.env.web2py_runtime_gae:
    ## if NOT running on Google App Engine use SQLite or other DB
    db = DAL(
        myconf.take('db_uri'), 
        fake_migrate_all=myconf.take('fake_migrate_all'), 
        driver_args=myconf.take('driver_args'),
        #pool_size=myconf.take('db.pool_size', cast=int), 
        check_reserved=['all']
    )
else:
    ## connect to Google BigTable (optional 'google:datastore://namespace')
    db = DAL('google:datastore')
    ## store sessions and tickets there
    session.connect(request, response, db=db)
    ## or store session in Memcache, Redis, etc.
    ## from gluon.contrib.memdb import MEMDB
    ## from google.appengine.api.memcache import Client
    ## session.connect(request, response, db = MEMDB(Client()))
current.db = db

## by default give a view/generic.extension to all actions from localhost
## none otherwise. a pattern can be 'controller/function.extension'
response.generic_patterns = ['*'] if request.is_local else []
## (optional) optimize handling of static files
# response.optimize_css = 'concat,minify,inline'
# response.optimize_js = 'concat,minify,inline'
## (optional) static assets folder versioning
# response.static_version = '0.0.0'
#########################################################################
## Here is sample code if you need for
## - email capabilities
## - authentication (registration, login, logout, ... )
## - authorization (role based authorization)
## - services (xml, csv, json, xmlrpc, jsonrpc, amf, rss)
## - old style crud actions
## (more options discussed in gluon/tools.py)
#########################################################################

from gluon.tools import Auth, Crud, Service, PluginManager, prettydate
auth = Auth(db)
crud, service, plugins = Crud(db), Service(), PluginManager()

## create all tables needed by auth if not custom tables
auth.define_tables(username=False, signature=False)

## configure email
mail = auth.settings.mailer
mail.settings.server = '172.17.0.1:25'
mail.settings.sender = 'dkcrobot@dkars.nl'
# mail.settings.login = 'username:password'

## configure auth policy
# auth.settings.registration_requires_verification = False
auth.settings.registration_requires_approval = True
auth.settings.reset_password_requires_verification = True
auth.settings.actions_disabled.append('register')

## if you need to use OpenID, Facebook, MySpace, Twitter, Linkedin, etc.
## register with janrain.com, write your domain:api_key in private/janrain.key
# from gluon.contrib.login_methods.rpx_account import use_janrain
# use_janrain(auth, filename='private/janrain.key')

#########################################################################
## Define your tables below (or better in another model file) for example
##
## >>> db.define_table('mytable',Field('myfield','string'))
##
## Fields can be 'string','text','password','integer','double','boolean'
##       'date','time','datetime','blob','upload', 'reference TABLENAME'
## There is an implicit 'id integer autoincrement' field
## Consult manual for more options, validators, etc.
##
## More API examples for controllers:
##
## >>> db.mytable.insert(myfield='value')
## >>> rows=db(db.mytable.myfield=='value').select(db.mytable.ALL)
## >>> for row in rows: print row.id, row.myfield
#########################################################################

## after defining tables, uncomment below to enable auditing
# auth.enable_record_versioning(db)

''' This table holds the contest dates '''
db.define_table('t_contests',
                Field('f_start', 'datetime', label=T('Start time')),
                Field('f_end', 'datetime', label=T('End time')),
                Field('f_closed', 'datetime', label=T('Close time')),
                Field('f_description', 'string', label=T('Description')),
                Field('f_published', 'boolean', label=T('Published')),
                plural='contests', singular='contest'
                )

''' This table holds the scores per contest and callsign '''
db.define_table('t_scores',
                Field('f_contest', db.t_contests, writable=False, readable=False),
                Field('f_callsign', 'string'),
                Field('f_category', 'string'),
                Field('f_total', 'integer'),
                Field('f_skipped', 'integer'),
                Field('f_points', 'integer'),
                Field('f_multiplier', 'integer'),
                Field('f_total_score', 'integer'),
                Field.Virtual('f_total_points', lambda row: row.t_scores.f_points * row.t_scores.f_multiplier)
                )

''' This table holds the Cabrillo log headers '''
db.define_table('t_logs',
                Field('f_contest', db.t_contests, writable=False, readable=False),
                Field('f_operator', 'string', label=T('Operator')),   # call from person who submitted the log file
                Field('f_category', 'string', label=T('Category')),   # compiled category according to contest rules
                Field('f_country', 'string', label=T('Country')),    # country for f_operator
                Field('f_soapbox', 'string', label=T('Soap box')),    # soap box
                Field('f_email', 'string', label=T('E-mail')),      # e-mail address
                Field('f_claimed_score', 'integer', label=T('Claimed score')), # claimed score
                Field('f_upload_ID', 'integer', readable=False), # original log file
                Field('f_swl', 'boolean', label=T('SWL')),
                plural='logs', singular='log'
                )

''' This table holds the actual QSO log entries '''
db.define_table('t_qsos',
                Field('f_contest', db.t_contests, writable=False, readable=False),
                Field('f_log', db.t_logs, writable=False, readable=False),
                Field('f_log_id', 'integer', readable=False),
                Field('f_from_callsign', 'string', label=T('Operator callsign')),
                Field('f_from_id', 'string', label=T('Matching QSO record')),
                Field('f_qsoline', 'string', label=T('Original QSO line')),
                Field('f_freq', 'integer', label=T('Frequency')),
                Field('f_mode', 'string', label=T('Mode')),
                Field('f_datetime', 'datetime', label=T('Date and time')),
                Field('f_callsign', 'string', label=T('Opponent callsign')),
                Field('f_exchange', 'string', label=T('Full exchange text')),
                Field('f_transmitter_id', 'string', label=T('Transmitter ID')),
                Field('f_info_sent', 'string', label=T('Info sent')),
                Field('f_info_received', 'string', label=T('Info received')),
                Field('f_country', 'string', label=T('Dxcc country')),
                Field('f_valid', 'boolean', label=T('Valid record')),
                Field('f_invalidation_reason', 'string', label=T('Invalidation reason')),
                plural='qsos', singular='qso'
                )

''' This table holds information on operators (callsigns) like their expat status '''
db.define_table('t_callsign',
                Field('f_callsign', 'string'),
                Field('f_expat_status', 'boolean'),
                Field('f_motivation', 'string')
                )

''' This table holds the category regions '''
db.define_table('t_category_regions',
                Field('f_region_code', 'string'),
                Field('f_region_name', 'string')
                )

''' This table holds the category modes '''
db.define_table('t_category_modes',
                Field('f_mode_code', 'string'),
                Field('f_mode_name', 'string')
                )

''' This table holds the category powers '''
db.define_table('t_category_powers',
                Field('f_power_code', 'string'),
                Field('f_power_name', 'string')
                )

''' This table holds the allowed categories '''
db.define_table('t_categories',
                Field('f_name', 'string'),
                Field('f_description', 'string', label=T('category'))
                )

''' This table holds the uploaded logs '''
db.define_table('t_uploads',
                Field('f_file', 'upload',
                      uploadfolder=os.path.join(request.folder, 'uploads'),
                      label=T('Log file to upload'), requires=IS_NOT_EMPTY()),
                Field('f_datetime', 'datetime',
                      readable=False, writable=False,
                      default=datetime.now()),
                Field('f_processed', 'boolean',
                      readable=False, writable=False,
                      default=False),
                Field('f_category_region', 'string', label=T('Category region'),
                      requires=IS_IN_DB(db, db.t_category_regions.f_region_code, '%(f_region_name)s', zero=None,
                                        orderby=db.t_category_regions.f_region_code)),
                Field('f_category_mode', 'string', label=T('Category mode'),
                      requires=IS_IN_DB(db, db.t_category_modes.f_mode_code, '%(f_mode_name)s', zero=None,
                                        orderby=db.t_category_modes.f_mode_code)),
                Field('f_category_power', 'string', label=T('Category type'),
                      requires=IS_IN_DB(db, db.t_category_powers.f_power_code, '%(f_power_name)s', zero=None,
                                        orderby=db.t_category_powers.f_power_code)),
                Field('f_email', 'string', label=T('e-Mail address'),
                      requires=IS_EMAIL()),
                Field('f_swl', 'boolean', label=T('SWL log')),
                Field('f_expat_motivation', 'string', label=T('Expat motivatie')),
                Field('f_mailinglist', 'boolean', label=T('Add to mailing list?'))
                )

''' This table holds the allowed bands.
An example:
20m, 14000, 14350
'''
db.define_table('t_bands',
                Field('f_name', 'string'),
                Field('f_low', 'integer'),
                Field('f_high', 'integer')
                )

''' This table links a contest to a list of bands '''
db.define_table('t_bands_for_contest',
                Field('f_contest',  # db.t_contests),
                      'integer', requires=IS_IN_DB(db, db.t_contests.id, '%(f_description)s', zero=None)),
                Field('f_band', 'integer',
                      requires=IS_IN_DB(db, db.t_bands.id, '%(f_name)s', zero=None)),
                )

''' This table holds the allowed modes (as defined by the Cabrillo standard):
PH (phone)
CW
'''
db.define_table('t_modes',
                Field('f_mode', 'string')
                )

